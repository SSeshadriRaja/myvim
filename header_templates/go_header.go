/***************************************************
 *
 * Name of the file : filename.go
 * Description      : Describe the usage of this file.
 * Engineer         : Seshadri Raja
 *
 ***************************************************/

// Package declaration
package main

// Factored import statements
import (
	"fmt"
)

func main() {
	fmt.Println("Gratitude.")
}
